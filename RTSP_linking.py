import cv2
import subprocess as sp
import logging

class RTSPStreamer:
    def __init__(
        self, app_name="testStream", server_ip="164.52.204.9", server_port=8554
    ):
        self.endpoint = f"rtsp://{server_ip}:{server_port}/{app_name}"
        self.process = None
        self.set_incoming_fps = 5
    def get_endpoint(self):
        return self.endpoint
    def __initialize_streamer(self, incoming_fps, target_fps, width, height):
        if self.process is not None:
            try:
                self.process.kill()
                self.process = None
                logging.info(f"RTSP Streamer Killed")
            except Exception as e:
                logging.warning(f"Error in killing process: {e}")
        self.set_incoming_fps = incoming_fps
        size_str = f"{width}x{height}"
        command = [
            "ffmpeg",
            "-re",
            "-s",
            size_str,
            "-r",
            str(incoming_fps),
            "-i",
            "-",
            "-pix_fmt",
            "yuv420p",
            "-r",
            str(target_fps),
            "-g",
            "50",
            "-c:v",
            "libx264",
            "-b:v",
            "2M",
            "-bufsize",
            "64M",
            "-maxrate",
            "4M",
            "-preset",
            "veryfast",
            "-rtsp_transport",
            "tcp",
            "-segment_times",
            "5",
            "-f",
            "rtsp",
            self.endpoint,
        ]
        self.process = sp.Popen(command, stdin=sp.PIPE)
        logging.info(f"RTSP Streamer Initialized")
    def __check_reinit(self, current_fps):
        if current_fps < self.set_incoming_fps / 2:
            logging.info(
                f"FPS Dropped Below Minimum Acceptable Threshold, Reinitializing Streamer"
            )
            return True
        else:
            return False
    def stream(self, frame, fps):
        height, width = frame.shape[:2]
        # Initialize Stream if Process does not exist or if FPS Drops to less than half
        if self.process is None or self.__check_reinit(fps):
            self.__initialize_streamer(fps, fps, width, height)
        ret, encoded_frame = cv2.imencode(".png", frame)
        if ret and self.process:
            self.process.stdin.write(encoded_frame.tobytes())
            return True
        else:
            self.__initialize_streamer(fps, fps, width, height)
            return False

if __name__ == "__main__":
    # from woutils.misc import set_fps, get_fps
    import time
    target_fps = 4
    fps = target_fps
    streamer = RTSPStreamer(app_name="har")
    file = "/home/arnav/Desktop/lights_off.mp4"
    cap = cv2.VideoCapture(file)
    while True:
        start_time = time.time()
        ret, frame = cap.read()
        if ret:
            res = streamer.stream(frame, fps)
        else:
            cap.release()
            cap = cv2.VideoCapture(file)
        print(fps, res, streamer.get_endpoint())